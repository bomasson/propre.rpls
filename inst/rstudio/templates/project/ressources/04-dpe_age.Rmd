# `r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["titre"]]` {#dpe}

## `r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["intertitre"]]` {.unnumbered}

`r creer_verbatim_4(data = indicateurs_rpls_ref, annee = annee)[["commentaires"]]`



```{r, warning=FALSE, message=FALSE}
creer_tableau_4_1(data = indicateurs_rpls_ref, annee = annee, epci = FALSE)
```

```{r DPE diag barres}
creer_graphe_4_1(data = indicateurs_rpls_ref, annee = annee)
```

