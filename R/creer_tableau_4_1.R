#' Creation du tableau sur l anciennete du parc (chapitre anciennete et etat energetique).
#'
#' @description Création du tableau 1 du chapitre 4 en html.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur et filtrée sur le booléen Zone_ref.
#' @param annee Une année parmi les millesimes sélectionnables par l'utilisateur, au format numerique.
#' @param epci un booléen pour indiquer si l'on souhaite détailler le tableau par EPCI.
#' @param add_scroll un booleen pour indique si l'on souhaite inserer une scrollbox.  (par défaut FALSE)
#' @param titre une chaine de caractère si vous voulez ajouter un titre spécifique. (par défaut: "Répartition des logements sociaux selon leur ancienneté au 1er janvier {annee}")
#' @param note_de_lecture une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources
#'
#' @return Un tableau mis en page au format html.
#'
#' @importFrom dplyr filter mutate arrange select pull
#' @importFrom forcats fct_relevel
#' @importFrom kableExtra kable kable_styling row_spec add_indent footnote scroll_box
#' @importFrom glue glue
#'
#' @export
#'
#' @examples
#' indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
#'  dplyr::filter(Zone_ref)
#'
#' creer_tableau_4_1(data = indicateurs_rpls_illustrations, annee = 2019, epci = FALSE,
#' add_scroll = FALSE, note_de_lecture = "")


creer_tableau_4_1 <- function(data, annee, epci = FALSE,
                              add_scroll = FALSE,
                              titre = NULL,
                              note_de_lecture = ""){

  if ( is.null(titre)){
    titre <- "R\u00e9partition des logements sociaux selon leur anciennet\u00e9<br>au 1er janvier {annee}"}

  # une fonction de calcul et de formatage des pourcentages du tableau
  mef <- function(x, y) {
    round(x / y * 100, 1) %>%
      format_fr_pct()
  }

  # Création du dataset utile pour la production du tableau
  tab <- data %>%
    # Filtre sur l'année N
    dplyr::filter(.data$millesime == annee) %>%
    # Modification de l'ordre des levels de la variable TypeZone
    dplyr::mutate(taux_ls_age_0_5 = mef(.data$nb_ls_age_0_5, .data$nb_ls_actif),
                  taux_ls_age_5_10 = mef(.data$nb_ls_age_5_10, .data$nb_ls_actif),
                  taux_ls_age_10_20 = mef(.data$nb_ls_age_10_20, .data$nb_ls_actif),
                  taux_ls_age_20_40 = mef(.data$nb_ls_age_20_40, .data$nb_ls_actif),
                  taux_ls_age_40_60 = mef(.data$nb_ls_age_40_60, .data$nb_ls_actif),
                  taux_ls_age_60_plus = mef(.data$nb_ls_age_60_plus, .data$nb_ls_actif)) %>%
    # Tri de la table pour faire apparaitre dans l'ordre FM/Région/Département/EPCI
    propre.rpls::arrange_zonage() %>%
    # Sélection des variables
    dplyr::select(.data$TypeZone, .data$Zone, .data$CodeZone, .data$millesime,
                  .data$taux_ls_age_0_5, .data$taux_ls_age_5_10, .data$taux_ls_age_10_20,
                  .data$taux_ls_age_20_40, .data$taux_ls_age_40_60, .data$taux_ls_age_60_plus)

  if(!epci){
    tab <- dplyr::filter(tab, .data$TypeZone != "Epci")
  }

  # Création du tableau
  tableau <- tab %>%
    # Sélection des variables utiles
    dplyr::select(.data$Zone, .data$taux_ls_age_0_5, .data$taux_ls_age_5_10, .data$taux_ls_age_10_20,
                  .data$taux_ls_age_20_40, .data$taux_ls_age_40_60, .data$taux_ls_age_60_plus) %>%
    # Mise en place des titres de colonnes
    kableExtra::kable("html", col.names=c("Zone",
                                          "Logements \u00e2g\u00e9s\nde moins\u00a0de\u00a05\u00a0ans",
                                          "Logements \u00e2g\u00e9s\nde 5\u00a0\u00e0\u00a09\u00a0ans",
                                          "Logements \u00e2g\u00e9s\nde 10\u00a0\u00e0\u00a019\u00a0ans",
                                          "Logements \u00e2g\u00e9s\nde 20\u00a0\u00e0\u00a039\u00a0ans",
                                          "Logements \u00e2g\u00e9s\nde 40\u00a0\u00e0\u00a059\u00a0ans",
                                          "Logements \u00e2g\u00e9s\nde plus\u00a0de\u00a060\u00a0ans"),
                      format.args = list(big.mark = " "),
                      align = "lrrrrrr",
                      caption = glue::glue(titre)) %>%
    # Formatage de la taille des caractères
    kableExtra::kable_styling(font_size = 12) %>%
    # Formatage de la ligne "R\u00e9gion" : fond blanc, gras
    kableExtra::row_spec(which(dplyr::pull(tab, .data$TypeZone) == "R\u00e9gions"), bold = TRUE, background = "#FFFFFF") %>%
    # Formatage des lignes "D\u00e9partements" : fond gris clair, gras
    kableExtra::row_spec(which(dplyr::pull(tab, .data$TypeZone) == "D\u00e9partements"), bold = TRUE, background = "#E5E5E5") %>%
    # Formatage des lignes "Epci" et "EPT" : fond blanc, taille 10
    kableExtra::row_spec(which(dplyr::pull(tab, .data$TypeZone) %in% c("EPT", "Epci")), bold = FALSE, background = "#FFFFFF",
                         font_size = 10) %>%
    # Ajout d'une indentation pour les lignes "Epci" et "EPT"
    kableExtra::add_indent(which(dplyr::pull(tab, .data$TypeZone)  %in% c("EPT", "Epci"))) %>%
    # Formatage des lignes "France" : fond gris foncé, gras
    kableExtra::row_spec(which(dplyr::pull(tab, .data$TypeZone) == "France"), bold = TRUE, background = "#C4C4C4") %>%
    # Création note de bas de page avec la source et l ajout parametrable d une note de lecture
    kableExtra::footnote(general = paste0(dplyr::if_else(note_de_lecture != "",
                                                         paste0(note_de_lecture, "\n"),
                                                         ""),
                                          caption(sources = 1, mil_rpls = annee)), general_title = "")

  # insere une scrollbox pour une meilleure lisibilite
  if (add_scroll) {
    tableau <- tableau %>%
      kableExtra::scroll_box(width = "100%", height = "500px", fixed_thead = TRUE)
  }

  return(tableau)

}
