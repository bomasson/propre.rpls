#' Ouvrir l'application de consultation des indicateurs complementaires
#' @param nom_reg Le nom de la région sur laquelle vous souhaitez que l'application se lance.
#' @param clean TRUE si vous voulez recalculer les données.
#' @importFrom rappdirs user_data_dir
#' @importFrom tidyr pivot_longer pivot_wider
#' @importFrom dplyr left_join mutate select bind_rows filter
#' @importFrom utils read.csv2
#' @return a shiny app
#' @export
#'

run_rpls_explorer <- function(nom_reg = "53 Bretagne", clean = FALSE) {

  df_path <- file.path(rappdirs::user_data_dir("propre.rpls"),"propre_rpls.RData")

  if (!file.exists(df_path) | clean) {

    if (!dir.exists(rappdirs::user_data_dir("propre.rpls"))) {dir.create(rappdirs::user_data_dir("propre.rpls"), recursive = TRUE)}

    liste_var_avec_libelle <- utils::read.csv2(propre.rpls::propre.rpls_file('rstudio/templates/project/ressources/extdata/dico_var.csv'), fileEncoding = "UTF-8")

    liste_var_avec_libelle_compl <- utils::read.csv2(propre.rpls::propre.rpls_file('rstudio/templates/project/ressources/extdata/dico_var_compl.csv'), fileEncoding = "UTF-8")

    liste_var_avec_libelle_rp <- utils::read.csv2(propre.rpls::propre.rpls_file('rstudio/templates/project/ressources/extdata/dico_var_rp.csv'), fileEncoding = "UTF-8")

    liste_var_avec_libelle <- dplyr::bind_rows(liste_var_avec_libelle, liste_var_avec_libelle_compl, liste_var_avec_libelle_rp) %>%
      dplyr::mutate(Chapitre = as.factor(.data$Chapitre))

    propre_rpls <- dataprep(nom_reg = nom_reg) %>%
      dplyr::filter((.data$TypeZone == "Communes" & .data$nb_logt_total > 0) | .data$TypeZone != "Communes") %>%
      tidyr::pivot_longer(-c(.data$TypeZone, .data$Zone, .data$Zone_ref, .data$CodeZone, .data$millesime),
                          names_to = "nom_court", values_to = "valeur") %>%
      dplyr::left_join(liste_var_avec_libelle %>% dplyr::select(.data$nom_court, .data$libelles)) %>%
      dplyr::select(-.data$nom_court) %>%
      tidyr::pivot_wider(names_from = "libelles", values_from = "valeur")

    save(propre_rpls, liste_var_avec_libelle, file = df_path)
  }

  appDir <- system.file("shiny", package = "propre.rpls")

  if (appDir == "") {
    stop("Could not find app directory. Try re-installing `{propre.rpls}`.", call. = FALSE)
  }

  shiny::runApp(appDir, display.mode = "normal")
}
