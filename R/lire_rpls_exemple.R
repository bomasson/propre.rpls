#' Lire exemple
#'
#' Permet de charger le jeu de donnees exemple.
#' Ce dernier contient les territoires de reference supra de la Corse (France metro et France de province), toutes les regions et tous les departements pour tous les millesimes
#' les communes corses pour 2020 (pour tester les cartes à la commune) et tous les EPCI corses pour tous les millesimes.
#'
#' @return un dataframe
#' @importFrom readr read_csv cols col_factor col_double col_logical
#' @export
#'
#' @examples
#' lire_rpls_exemple()

lire_rpls_exemple <- function(){
  file <- propre.rpls::propre.rpls_file("data-examples/indicateurs_rpls_exemple.csv")
  readr::read_csv(file,
                  col_types = readr::cols(
                    .default = readr::col_double(),
                    TypeZone = readr::col_factor(levels = NULL, ordered = FALSE, include_na = FALSE),
                    Zone = readr::col_factor(levels = NULL, ordered = FALSE, include_na = FALSE),
                    CodeZone = readr::col_factor(levels = NULL, ordered = FALSE, include_na = FALSE),
                    millesime = readr::col_factor(levels = NULL, ordered = FALSE, include_na = FALSE),
                    Zone_ref = readr::col_logical()
                  )
  )
}
