#' Chapitre 6: Verbatim
#'
#' @description Production des commentaires et de l'intertitre verbatim du chapitre 6.

#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur
#' @param annee Le millesime renseigné par l'utilisateur (au format numérique)
#'
#' @return Un vecteur de 2 chaînes de caractères comprenant l'intertitre et les commentaires essentiels du chapitre 6
#'
#' @importFrom dplyr filter select transmute pull contains mutate case_when starts_with across everything
#' @importFrom glue glue
#' @importFrom tidyr pivot_wider
#' @importFrom propre.datareg datareg
#'
#' @export
#'
#' @examples
#' indic_rpls <- propre.rpls::lire_rpls_exemple()
#'
#' creer_verbatim_6(data = indic_rpls, annee = 2019)[["intertitre"]]
#' creer_verbatim_6(data = indic_rpls, annee = 2019)[["commentaires"]]





creer_verbatim_6 <- function(data, annee) {

  # on calcule d'abord les indicateurs necessaires aux commentaires
  mises_svc <- data %>%
    # on garde les lignes pertinentes : Fce metro si reg metropolitaine (CodeZone == FRMETRO) ou Fce entiere si DROM (CodeZone == FRMETRODROM)
    dplyr::filter(.data$millesime == annee, grepl("gion", .data$TypeZone) | grepl("FRMETRO", .data$CodeZone), .data$Zone_ref) %>%
    dplyr::select(.data$millesime, .data$TypeZone, .data$nb_plai, .data$nb_mes, .data$nb_plus) %>%
    dplyr::transmute(TypeZone = substr(.data$TypeZone, 1, 1), # on simplifie le lib de niv geo pour limiter les soucis d'encodage
                  part_plai = round(.data$nb_plai / .data$nb_mes * 100, 1),
                  part_plus = round(.data$nb_plus / .data$nb_mes * 100, 1)) %>%
    tidyr::pivot_wider(names_from = .data$TypeZone, values_from = c(.data$part_plai, .data$part_plus))

  # une variable pour recuperer l'identifiant de la région choisie
  id_reg <- dplyr::filter(data, .data$Zone_ref, grepl("gion", .data$TypeZone)) %>%
    dplyr::pull(.data$CodeZone) %>% unique() %>% as.character

  # un booleen pour determiner si la region choisie est metropolitaine ou non
  metro <- !(id_reg %in% paste0("0", 1:6))

  loyers_0 <- data %>%
    dplyr::filter(.data$millesime == annee, grepl("gions", .data$TypeZone) | grepl("FRMETRO", .data$CodeZone)) %>%
    dplyr::select(dplyr::contains("Zone"), .data$somme_loyer, .data$somme_surface, .data$somme_loyer_recent, .data$somme_surface_recent,
                  .data$somme_loyer_nb_plai, .data$somme_surface_nb_plai, .data$somme_loyer_nb_pls, .data$somme_surface_nb_pls) %>%
    dplyr::mutate(TypeZone = substr(.data$TypeZone, 1, 1),
                  reg_comp = dplyr::case_when(
                    .data$TypeZone != "R" ~ FALSE,
                    .data$TypeZone == "R" & metro & .data$CodeZone %in% paste0("0", 1:6) ~ FALSE,
                    .data$TypeZone == "R" & metro & !(.data$CodeZone %in% paste0("0", 1:6)) ~ TRUE,
                    .data$TypeZone == "R" & !metro & .data$CodeZone %in% paste0("0", 1:6) ~ TRUE,
                    .data$TypeZone == "R" & !metro & !(.data$CodeZone %in% paste0("0", 1:6)) ~ FALSE,
                    TRUE ~ FALSE),
                  loyer_moy = .data$somme_loyer / .data$somme_surface ,
                  loyer_moy_recent = .data$somme_loyer_recent / .data$somme_surface_recent ,
                  loyer_moy_plai = .data$somme_loyer_nb_plai / .data$somme_surface_nb_plai ,
                  loyer_moy_pls = .data$somme_loyer_nb_pls / .data$somme_surface_nb_pls) %>%
    dplyr::select(-dplyr::starts_with("somme_"))

  loyers_rang <- loyers_0 %>%
    dplyr::filter(.data$reg_comp) %>%
    dplyr::select(.data$Zone_ref, .data$loyer_moy) %>%
    dplyr::mutate(rang = rank(-.data$loyer_moy),
                  dernier = (.data$rang == nrow(.)),
                  rang_loy = dplyr::case_when(
                    .data$rang == 1 ~ "1er",
                    .data$dernier ~ "dernier",
                    TRUE ~ paste0(.data$rang, "e"))) %>%
    dplyr::filter(.data$Zone_ref) %>%
    dplyr::pull(.data$rang_loy)


  loyers <- loyers_0 %>%
    dplyr::filter(.data$Zone_ref) %>%
    dplyr::select(.data$TypeZone, dplyr::starts_with("loyer_moy")) %>%
    tidyr::pivot_wider(names_from = .data$TypeZone, values_from = dplyr::starts_with("loyer_moy")) %>%
    dplyr::mutate(dplyr::across(dplyr::everything(), format_fr_nb))


  # on récupère les formulations idiomatiques grâce à {propre.datareg}
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)


  # on cree ensuite une liste nommee des differents parametres
  verb6 <- list(part_mes_plai_r = mises_svc$part_plai_R[1] %>% format_fr_pct,
                part_mes_plus_r = mises_svc$part_plus_R[1] %>% format_fr_pct,
                annee_prec = annee - 1,
                dans_la_region = verb_reg$dans_la_region,
                part_mes_plai_fr = mises_svc$part_plai_F[1] %>% format_fr_pct,
                comp_fr_mes_plai = ifelse(metro, "France m\u00e9tropolitaine", "France enti\u00e8re"),
                loyer_moy_r = loyers$loyer_moy_R,
                loyer_moy_fr = loyers$loyer_moy_F,
                reg_dep = verb_reg$la_region,
                rang = loyers_rang,
                reg_rang = ifelse(metro, "des r\u00e9gions les plus ch\u00e8res de m\u00e9tropole", "des d\u00e9partements les plus chers d\u2019outre-mer"),
                loy_moy_recent_r = loyers$loyer_moy_recent_R,
                comp_fr_loyer = ifelse(metro, "En France m\u00e9tropolitaine", "Sur l\'ensemble de la France"),
                loy_moy_recent_fr = loyers$loyer_moy_recent_F,
                comp_fr_loy_recent = ifelse(metro, "en France m\u00e9tropolitaine", "sur l\'ensemble de la France"),
                loyer_moy_plai_r = loyers$loyer_moy_plai_R,
                loyer_moy_pls_r = loyers$loyer_moy_pls_R,
                nom_plus_long = ifelse(metro, "PLUS (Pr\u00eat locatif \u00e0 usage social)", "LLS (Logement locatif social)"),
                nom_plai_long = ifelse(metro, "PLAI (pr\u00eat locatif aid\u00e9 d\u2019int\u00e9gration)", "LLTS (Logement locatif tr\u00e8s social)"),
                nom_plus_court = ifelse(metro, "PLUS", "LLS"),
                nom_plai_court = ifelse(metro, "PLAI", "LLTS")
                )


  # production du verbatim a partir des elements precedents
  verbatim_chap_6 <- list(intertitre ="", commentaires ="")
  verbatim_chap_6$intertitre <- glue::glue("{verb6$part_mes_plai_r} des mises en service \u00e0 destination des plus pr\u00e9caires")
  verbatim_chap_6$commentaires <- glue::glue(
  "Le {verb6$nom_plus_long} finance {verb6$part_mes_plus_r} des logements mis en service en {verb6$annee_prec} {verb6$dans_la_region}.
  Le {verb6$nom_plai_long} finance des logements \u00e0 destination des publics les plus en difficult\u00e9s. Il a concern\u00e9
  {verb6$part_mes_plai_r} des mises en service en {verb6$annee_prec} {verb6$dans_la_region} contre {verb6$part_mes_plai_fr} en {verb6$comp_fr_mes_plai}.

  Le loyer moyen s\u2019\u00e9l\u00e8ve \u00e0 {verb6$loyer_moy_r}\u202f\u20ac/m\u00b2 en {annee}, ce qui situe {verb6$reg_dep} au {verb6$rang} rang {verb6$reg_rang}.
  {verb6$comp_fr_loyer}, le loyer moyen s\u2019affiche \u00e0 {verb6$loyer_moy_fr}\u202f\u20ac/m\u00b2.

  Dans le parc r\u00e9cent (mis en service depuis 5 ans ou moins), le loyer moyen s\u2019\u00e9tablit \u00e0 {verb6$loy_moy_recent_r}\u202f\u20ac/m\u00b2 contre
  {verb6$loy_moy_recent_fr}\u202f\u20ac/m\u00b2 {verb6$comp_fr_loy_recent}.

  Le loyer moyen est par ailleurs conditionn\u00e9 par le mode de financement initial. Il s\u2019affiche \u00e0 {verb6$loyer_moy_plai_r}\u202f\u20ac/m\u00b2 pour
  les logements financ\u00e9s par un {verb6$nom_plai_court} et {verb6$loyer_moy_pls_r}\u202f\u20ac/m\u00b2 pour ceux financ\u00e9s par un PLS."
  )

  verbatim_chap_6
}
