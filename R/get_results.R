#' get_dataprep : recuperer les resultats de la precedente execution de la datapreparation
#'
#' Cette fonction evite de lancer plusieurs fois la fonction de preparation des données `dataprep()` en sauvegardant le resultat de son execution.
#' Cela pouvait s'averer fastidieux en cas de compilations repetees de la publication.
#' La datapreparation ne s'execute qu'en cas de 1ere execution ou de souhait de mise a jour des indicateurs
#' (utile si par exemple le package a evolue au niveau des donnees, ou si on change des parametres utilisateurs comme la liste des EPCI de reference, la region ou le millesime).
#'
#' @param maj Booleen pour indiquer si on souhaite mettre a jour les donnees en relancant la fonction de datapreparation.
#' @param par_util La liste des parametres utilisateurs de la publication (params).
#' @param test Booleen qui indique si on souhaite executer la version de test de datapreparation.
#' @param ext_dir Le repertoire d'export des donnees en RData, sera cree si inexistant. Par defaut le repertoire de travail courant.
#'
#' @return Un dataframe d'indicateurs : une ligne par entite geographique et millesime, le champ Zone_ref permet de filtrer les territoires pour ne conserver que ceux a inclure dans les illustrations.
#' @export
#'
#' @examples
#' get_dataprep(maj = FALSE, test = TRUE, ext_dir = tempdir(),
#'              par_util = list(nom_region = "Bretagne", epci_ref = "3- ", epci_list = NULL))
get_dataprep <- function(maj = FALSE, par_util = list(nom_region = "Bretagne", epci_ref = "3- ", epci_list = NULL),
                         test = FALSE, ext_dir = ".")  {
  dir.create(ext_dir, recursive = TRUE, showWarnings = FALSE)
  dataset <- "indicateurs_rpls"
  rdata <- paste0(ext_dir, "/", dataset, ".RData")

  if(file.exists(rdata) & !maj) {
    load(rdata)
    return(indicateurs_rpls)
  } else {
    df <- propre.rpls::dataprep(par_util$nom_region, choix_epci = par_util$epci_ref, epci_list = par_util$epci_list, test = test)
    assign(dataset, df)
    save("indicateurs_rpls", file = rdata)
    return(df)
  }
}

#' get_fond_carto : recuperer les resultats de la precedente execution de la preparation du fond carto
#'
#' Cette fonction evite de lancer plusieurs fois la fonction de preparation du fond carto `fond_carto()`, en exportant le resultat de son execution.
#' Cela pouvait s'averer fastidieux en cas de compilations repetees de la publication.
#' La preparation du fond carto ne s'execute qu'en cas de 1ere execution ou de souhait de mise a jour.
#'
#' @param maj Booleen pour indiquer si on souhaite mettre a jour les donnees en relancant la fonction de datapreparation.
#' @param par_util La liste des parametres utilisateurs de la publication (params).
#' @param ext_dir Le repertoire d'export des donnees en RData, sera cree si inexistant. Par defaut le repertoire de travail courant.
#' @param ... Autres parametres de la fonction  \code{mapfactory::\link{fond_carto}}.
#'
#' @return Le fond_carto, ie une liste de dataframes geographiques sf.
#' @export
#'
#' @examples
#' get_fond_carto(maj = FALSE, par_util = list(nom_region = "Bretagne"), ext_dir = tempdir())
get_fond_carto <- function(maj = FALSE, par_util = list(nom_region = "Bretagne"), ext_dir = ".", ...) {
  dir.create(ext_dir, recursive = TRUE, showWarnings = FALSE)
  dataset <- "fond_carto"
  rdata <- paste0(ext_dir, "/", dataset, ".RData")
  if(file.exists(rdata) & !maj) {
    load(rdata)
    return(fond_carto)
  } else {
    df <- mapfactory::fond_carto(par_util$nom_region, ...)
    assign(x = dataset, value = df)
    save("fond_carto", file = rdata)
    return(df)
  }
}

