build_results <- function(data, millesime){

  table <- data %>% select(DEPCOM) %>% unique() %>%
    mutate(millesime = millesime)

  # Nombre de logements dans le parc des bailleurs sociaux (social + privé)
  # On calcule cette variable avant de passer toute la table au format décret
  table <- data %>%
    dplyr::filter(MODE != "9") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_logt_total") %>%
    left_join(table, ., by = "DEPCOM")

  data <- data %>% toDecret() %>%
    dplyr::mutate(age = if_else(!is.na(CONSTRUCT), millesime - 1 - as.numeric(CONSTRUCT), NA_real_),
                  finan_cus = case_when(FINAN %in% c("10", "11") ~ "PLAI",
                                        FINAN %in% c("12", "13", "50":"53") ~ "PLUS",
                                        FINAN %in% c("49", "54", "55", "99") & CONV == "1" ~ "PLUS",
                                        FINAN %in% c("14", "15") ~ "PLS",
                                        FINAN == "17" & CONV == "1" ~ "PLS",
                                        FINAN == "16" ~ "PLI",
                                        FINAN %in% c("17", "49", "54", "55", "99") & CONV == "2" ~ "PLI"),
                  LOYERPRINC = as.numeric(LOYERPRINC),
                  SURFHAB = as.numeric(SURFHAB),
                  loymoy = if_else(!is.na(LOYERPRINC) & LOYERPRINC  %()% c(0, 9999) & !is.na(SURFHAB),
                                   round(LOYERPRINC/SURFHAB, 2), NA_real_ )

    )

  quartiles <- quantile(data %>% dplyr::filter(MODE == 1) %>% pull(loymoy), na.rm = T)
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # Nombre de logements sociaux au actifs 1er janvier N ####
  table <- data %>% dplyr::filter(MODE != "9") %>% group_by(DEPCOM) %>%
    count(name = "nb_ls_actif") %>%
    left_join(table, ., by = "DEPCOM")

  # QPV : nombre de logement hors QPV = nb_ls_actifs - nb_ls_qpv
  table <- data %>% filter(MODE != "9" & QPV == "1") %>% group_by(DEPCOM) %>%
    count() %>%
    rename(`nb_ls_qpv` = n) %>%
    left_join(table, ., by = "DEPCOM")

  # Type de construction
  # Modalité étudiants ajoutée après 2015
  if (millesime <= 2015){
    table <- data %>% filter(MODE != "9") %>%
      group_by(DEPCOM, TYPECONST) %>%
      count() %>%
      spread(TYPECONST, n) %>%
      mutate(E = NA) %>%
      rename(nb_ls_coll = `C`,
             nb_ls_ind = `I`,
             nb_ls_etu = `E`) %>%
      left_join(table, ., by = "DEPCOM")
  } else {
    table <- data %>% filter(MODE != "9") %>% group_by(DEPCOM, TYPECONST) %>% count() %>%
      spread(TYPECONST, n) %>%
      rename(nb_ls_coll = `C`,
             nb_ls_ind = `I`,
             nb_ls_etu = `E`) %>%
      left_join(table, ., by = "DEPCOM")
  }


  # Catégorie d'organisme propriétaire : DC (Données complémentaires)
  table <- data %>% filter(MODE != "9") %>% group_by(DEPCOM, CAT_ORG2) %>% count() %>%
    spread(CAT_ORG2, n) %>%
    rename(nb_ls_oph = OPH,
           nb_ls_esh = ESH,
           nb_ls_sem = SEM,
           nb_ls_autres_cat = Autres) %>%
    left_join(table, ., by = "DEPCOM")

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 2 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # Nombre de pièces
  table <- data %>% filter(MODE != "9") %>%
    mutate(NBPIECE = if_else(NBPIECE %in% c(5:9), "nb_piece_5_plus", as.character(NBPIECE))) %>%
    group_by(DEPCOM, NBPIECE) %>% count() %>%
    spread(NBPIECE, n) %>%
    rename(nb_piece_1 = `1`,
           nb_piece_2 = `2`,
           nb_piece_3 = `3`,
           nb_piece_4 = `4`) %>%
    left_join(table, ., by = "DEPCOM")

  # Nombre de pièces parc récent
  table <- data %>% filter(MODE != "9" & LOCAT >= (millesime - 5)) %>%
    mutate(NBPIECE = if_else(NBPIECE %in% c(5:9), "nb_piece_5_plus_recent", as.character(NBPIECE))) %>%
    group_by(DEPCOM, NBPIECE) %>% count() %>%
    spread(NBPIECE, n) %>%
    rename(nb_piece_1_recent = `1`,
           nb_piece_2_recent = `2`,
           nb_piece_3_recent = `3`,
           nb_piece_4_recent = `4`) %>%
    left_join(table, ., by = "DEPCOM")

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 3 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Nombre de MES
  table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1) %>%
    group_by(DEPCOM) %>% count() %>%
    rename(nb_mes = n) %>%
    left_join(table, ., by = "DEPCOM")

  # Nombre de MES par origine
  # table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1) %>%
  #   group_by(DEPCOM, ORIGINE) %>% count() %>%
  #   spread(ORIGINE, n) %>%
  #   rename(nb_mes_construit_org = `1`,
  #          nb_mes_acq_av_travaux = `2`,
  #          nb_mes_acq_ss_travaux = `3`,
  #          nb_mes_acq_vefa = `4`) %>%
  #   left_join(table, ., by = "DEPCOM")

  # Nombre de MES par type de bailleur (3.1)
  table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1) %>%
    group_by(DEPCOM, CAT_ORG2) %>% count() %>%
    spread(CAT_ORG2, n) %>%
    rename(nb_mes_oph = OPH,
           nb_mes_esh = ESH,
           nb_mes_sem = SEM,
           nb_mes_autres_cat = Autres) %>%
    left_join(table, ., by = "DEPCOM")

  # Nombre de MES par origine et en QPV
  # Les QPV sont apparus dans le répertoire en 2016
  if (millesime < 2016){
    table <- table %>%
      mutate(nb_mes_qpv_construit_org = NA,
             nb_mes_qpv_acq_av_travaux = NA,
             nb_mes_qpv_acq_ss_travaux = NA,
             nb_mes_qpv_acq_vefa = NA)
  } else {
    table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1 & QPV == "1") %>%
      group_by(DEPCOM, ORIGINE) %>% count() %>%
      spread(ORIGINE, n) %>%
      rename(nb_mes_qpv_construit_org = `1`,
             nb_mes_qpv_acq_av_travaux = `2`,
             nb_mes_qpv_acq_ss_travaux = `3`,
             nb_mes_qpv_acq_vefa = `4`) %>%
      left_join(table, ., by = "DEPCOM")
  }


  # Nombre de MES par origine et hors QPV
  if (millesime < 2016){
    table <- table %>%
      mutate(nb_mes_nonqpv_construit_org = NA,
             nb_mes_nonqpv_acq_av_travaux = NA,
             nb_mes_nonqpv_acq_ss_travaux = NA,
             nb_mes_nonqpv_acq_vefa = NA)
  } else {
    table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1 & QPV == "2") %>%
      group_by(DEPCOM, ORIGINE) %>% count() %>%
      spread(ORIGINE, n) %>%
      rename(nb_mes_nonqpv_construit_org = `1`,
             nb_mes_nonqpv_acq_av_travaux = `2`,
             nb_mes_nonqpv_acq_ss_travaux = `3`,
             nb_mes_nonqpv_acq_vefa = `4`) %>%
      left_join(table, ., by = "DEPCOM")
  }


  # Nombre de MES par financement
  table <- data %>% filter(MODE != "9" & MES_SANSCUMUL == 1) %>%
    mutate(FINAN = paste0("nb_", tolower(finan_cus))) %>%
    group_by(DEPCOM, FINAN) %>% count() %>%
    spread(FINAN, n) %>%
    left_join(table, ., by = "DEPCOM")

  # Equation comptable : NB LS, MES, SORTIES, ventes autre bailleur
  table <- data %>% filter(MODE == "9" & SORTIEPATRIM != "9") %>%
    # On regroupe les "autres" ventes de logements sociaux
    mutate(SORTIEPATRIM = if_else(SORTIEPATRIM %in% c("1", "3"), "1", SORTIEPATRIM)) %>%
    group_by(DEPCOM, SORTIEPATRIM) %>% count() %>%
    spread(SORTIEPATRIM, n) %>%
    rename(nb_ventes = `1`,
           nb_vente_bailleur = `2`,
           nb_demolition = `4`,
           nb_sorties_autres_motifs = `5`) %>%
    left_join(table, ., by = "DEPCOM")

  # changements d'usages / fusions-scissions
  table <- data %>%
    filter(MODE == "9" & OLDLOGT %not_in% c("", NA)) %>%
    mutate(OLDLOGT = if_else(OLDLOGT %in% c("1", "2"), "nb_oldlogt", OLDLOGT)) %>%
    group_by(DEPCOM, OLDLOGT) %>% count() %>%
    spread(OLDLOGT, n) %>%
    left_join(table, ., by = "DEPCOM")

  # browser()
  table <- table %>%
    dplyr::mutate_if(is.numeric, replace_na, 0) %>%
    mutate(nb_sorties_autres_motifs = nb_sorties_autres_motifs + nb_oldlogt) %>%
    select(-nb_oldlogt)

  # Rattrapages de collecte
  # Nombre de logements N - Nombre de logements N-1 - Nombre de mises en service en N - Nombre de logements sortis

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # Ancienneté des logements
  # Création de la variable age en fonction du millésime
  data <- data %>%
    mutate(age2 = case_when(age < 5 ~ 1,
                            age >= 5 & age < 10 ~ 2,
                            age >= 10 & age < 20 ~ 3,
                            age >= 20 & age < 40 ~ 4,
                            age >= 40 & age < 60 ~ 5,
                            age >= 60 ~ 6)
    )

  table <- data %>% filter(MODE != "9") %>%
    group_by(DEPCOM, age2) %>% count() %>%
    spread(age2, n) %>%
    rename(nb_ls_age_0_5 = `1`,
           nb_ls_age_5_10 = `2`,
           nb_ls_age_10_20  = `3`,
           nb_ls_age_20_40 = `4`,
           nb_ls_age_40_60 = `5`,
           nb_ls_age_60_plus = `6`) %>%
    left_join(table, ., by = "DEPCOM")


  # DPE énergie
  table <- data %>% filter(MODE != "9" & DPEENERGIE %not_in% c(NA, "") & DPEDATE %not_in% c("", "01/1900", NA)) %>%
             group_by(DEPCOM, DPEENERGIE) %>% count() %>%
             spread(DPEENERGIE, n) %>%
             rename(nb_ls_dpe_ener_A = `A`,
                    nb_ls_dpe_ener_B = `B`,
                    nb_ls_dpe_ener_C = `C`,
                    nb_ls_dpe_ener_D = `D`,
                    nb_ls_dpe_ener_E = `E`,
                    nb_ls_dpe_ener_F = `F`,
                    nb_ls_dpe_ener_G = `G`) %>%
             left_join(table, ., by = "DEPCOM")

  # DPE serre
  table <- data %>% filter(MODE != "9" & DPESERRE %not_in% c(NA, "") & DPEDATE %not_in% c("", "01/1900", NA)) %>%
    group_by(DEPCOM, DPESERRE) %>% count() %>%
    spread(DPESERRE, n) %>%
    rename(nb_ls_dpe_serre_A = `A`,
           nb_ls_dpe_serre_B = `B`,
           nb_ls_dpe_serre_C = `C`,
           nb_ls_dpe_serre_D = `D`,
           nb_ls_dpe_serre_E = `E`,
           nb_ls_dpe_serre_F = `F`,
           nb_ls_dpe_serre_G = `G`) %>%
    left_join(table, ., by = "DEPCOM")



  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 5 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # Mode d'occupation
  table <- data %>% filter(MODE != "9" ) %>%
    mutate(MODE = if_else(MODE == "6", "5", MODE)) %>%
    group_by(DEPCOM, MODE) %>% count() %>%
    spread(MODE, n) %>%
    rename(nb_ls_loue = `1`,
           nb_ls_vacant = `2`,
           nb_ls_vide = `3`,
           nb_ls_association = `4`,
           nb_ls_autre = `5`) %>%
    left_join(table, ., by = "DEPCOM")

  # Taux de vacance : nb de ls loués / nb ls loués + vacants


  # Nombre de logements vacants depuis plus de 3 mois

  # Calcul de la durée de vacance~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  data <- data %>%
    mutate(duree_vacance = NA_real_,
           ANNEE_REMLOC  = str_sub(REMLOCDATE, 4, 7),
           MOIS_REMLOC   = str_sub(REMLOCDATE, 1, 2),
           MOIS_BAIL = str_sub(BAIL, 1, 2),
           ANNEE_BAIL = str_sub(BAIL, 4, 7)
    )

  data <- data %>%
    mutate(BAIL_DATE = if_else(MODE == "1" & BAIL != "", lubridate::dmy(str_c("01",MOIS_BAIL, ANNEE_BAIL)), NA_Date_ ),
           REMLOCDATE_DATE = if_else(MODE %in% c("1", "2") & REMLOCDATE != "", dmy(str_c("01",MOIS_REMLOC, ANNEE_REMLOC)), NA_Date_))


  collecte_date <- zoo::as.yearmon(lubridate::dmy(str_c("0101", millesime)), format = "%m%Y")


  data <- data %>%
    mutate(REMLOCDATE_DATE = if_else(REMLOCDATE != "" & !is.na(CONSTRUCT) & ANNEE_REMLOC < CONSTRUCT, NA_Date_, REMLOCDATE_DATE),
           REMLOCDATE = if_else(REMLOCDATE != "" & !is.na(CONSTRUCT) & ANNEE_REMLOC < CONSTRUCT, "", REMLOCDATE),

           REMLOCDATE_DATE = if_else(MODE == "1" & REMLOCDATE != "" & BAIL != "" & ANNEE_BAIL < ANNEE_REMLOC, NA_Date_, REMLOCDATE_DATE),
           REMLOCDATE = if_else(MODE == "1" & REMLOCDATE != "" & BAIL != "" & ANNEE_BAIL < ANNEE_REMLOC, "", REMLOCDATE)
    )

  # Durée de vacance en mois
  data <- data %>%
    mutate(duree_vacance = if_else(MODE == "1" & BAIL != "" & REMLOCDATE != "",
                                   as.numeric(zoo::as.yearmon(BAIL_DATE, format = "%m%Y")-zoo::as.yearmon(REMLOCDATE_DATE, format = "%m%Y"))*12,
                                   NA_real_))

  data <- data %>%
    mutate(duree_vacance = if_else(MODE == "2" & REMLOCDATE != "",
                                   as.numeric((collecte_date-zoo::as.yearmon(REMLOCDATE_DATE, format = "%m%Y"))) * 12,
                                   duree_vacance))

  # éééééééééééééééééééééééééééééééééééééééééé
  #
  # data <- data %>%
  #   mutate(BAIL_DATE = if_else(MODE %in% "1" & !is.na(BAIL), dmy(str_c("01",MOIS_BAIL, ANNEE_BAIL)), dmy(str_c("01","01", "1900"))))
  #
  # data <- data %>%
  #   mutate(REMLOCDATE_DATE = if_else(MODE %in% c("1", "2") & !is.na(REMLOCDATE),dmy(str_c("01",MOIS_REMLOC, ANNEE_REMLOC)),dmy(str_c("01","01", "2900"))))
  #
  #
  # data <- data %>%
  #   mutate(duree_vacance = if_else(MODE %in% "1" & BAIL_DATE != "1900-01-01" & REMLOCDATE_DATE != "2900-01-01",
  #                                  as.numeric(zoo::as.yearmon(BAIL_DATE, format = "%d%m%Y")-zoo::as.yearmon(REMLOCDATE_DATE, format = "%d%m%Y"))*12,
  #                                  1000))
  #
  #
  # collecte_date <- zoo::as.yearmon(dmy(str_c("0101", millesime)), format = "%d%m%Y")
  #
  #
  # data <- data %>%
  #   mutate(duree_vacance = if_else(.$MODE %in% "2" & .$REMLOCDATE %not_in% c(NA, ""),
  #                                  as.numeric((collecte_date-zoo::as.yearmon(REMLOCDATE_DATE, format = "%d%m%Y")))*12,
  #                                  duree_vacance))
  #
  #
  # data <- data %>%
  #   mutate(duree_vacance = if_else(duree_vacance > 600, NA_real_, duree_vacance))

  # data[data$duree_vacance == 1000, "duree_vacance"] = NA

  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  table <- data %>%
    filter(duree_vacance > 3 & MODE == "2" & !is.na(duree_vacance)) %>%
    group_by(DEPCOM) %>% count(name = "nb_ls_vacant_3") %>%
    left_join(table, ., by = "DEPCOM")

  # Taux de mobilité = nb de logements ayant changé d'occupant/nombre de logements loués + vacants non mis en service
  table <- data %>%
    mutate(annee_bail = lubridate::year(mdy(BAIL))) %>%
    filter(MODE == "1" & annee_bail == (millesime - 1) & LOCAT %not_in% c(millesime, millesime - 1)) %>%
    group_by(DEPCOM) %>% count(name = "num_mob") %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    filter(MODE %in% c("1", "2") & LOCAT %not_in% c(millesime, millesime - 1)) %>%
    group_by(DEPCOM) %>% count(name = "denom_mob") %>%
    left_join(table, ., by = "DEPCOM")


  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Chapitre 6 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # Loyer moyen par financement mises en service
  table <- data %>%
    filter(MODE == "1" & MES_SANSCUMUL == 1 & !is.na(SURFHAB)) %>%
    mutate(FINAN = paste0("somme_loyer_nb_", tolower(finan_cus), "_mes"),
           LOYERPRINC = as.numeric(LOYERPRINC)) %>%
    group_by(DEPCOM, FINAN) %>%
    summarise(somme_loyer_mes = sum(LOYERPRINC, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_loyer_mes) %>%
    left_join(table, ., by = "DEPCOM")


  table <- data %>%
    filter(MODE == "1" & MES_SANSCUMUL == 1 & !is.na(LOYERPRINC)) %>%
    mutate(FINAN = paste0("somme_surface_nb_", tolower(finan_cus), "_mes"),
           SURFHAB = as.numeric(SURFHAB)) %>%
    group_by(DEPCOM, FINAN) %>%
    summarise(somme_surface_mes = sum(SURFHAB, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_surface_mes) %>%
    left_join(table, ., by = "DEPCOM")

  # Loyer moyen par financement parc récent
  table <- data %>%
    filter(MODE == "1" & LOCAT >= c(millesime - 5) & !is.na(SURFHAB)) %>%
    mutate(FINAN = paste0("somme_loyer_nb_", tolower(finan_cus), "_recent"),
           LOYERPRINC = as.numeric(LOYERPRINC)) %>%
    group_by(DEPCOM,FINAN) %>%
    summarise(somme_loyer_recent = sum(LOYERPRINC, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_loyer_recent) %>%
    left_join(table, ., by = "DEPCOM")


  table <- data %>%
    filter(MODE == "1" & LOCAT >= c(millesime - 5) & !is.na(LOYERPRINC)) %>%
    mutate(FINAN = paste0("somme_surface_nb_", tolower(finan_cus), "_recent"),
           SURFHAB = as.numeric(SURFHAB)) %>%
    group_by(DEPCOM, FINAN) %>%
    summarise(somme_surface_recent = sum(SURFHAB, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_surface_recent) %>%
    left_join(table, ., by = "DEPCOM")

  # Loyer moyen par financement parc complet
  table <- data %>%
    filter(MODE == "1" & !is.na(SURFHAB)) %>%
    mutate(FINAN = paste0("somme_loyer_nb_", tolower(finan_cus)),
           LOYERPRINC = as.numeric(LOYERPRINC)) %>%
    group_by(DEPCOM, FINAN) %>%
    summarise(somme_loyer_finan = sum(LOYERPRINC, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_loyer_finan) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    filter(MODE == "1" & !is.na(LOYERPRINC)) %>%
    mutate(FINAN = paste0("somme_surface_nb_", tolower(finan_cus)),
           SURFHAB = as.numeric(SURFHAB)) %>%
    group_by(DEPCOM, FINAN) %>%
    summarise(somme_surface_finan = sum(SURFHAB, na.rm = T), .groups = "drop") %>%
    spread(FINAN, somme_surface_finan) %>%
    left_join(table, ., by = "DEPCOM")



  # Données complémentaires ########################################

  # part des logts individuels parmi les LS récents xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime - 5) & TYPECONST == "I") %>%
    dplyr::group_by(DEPCOM) %>%
    dplyr::count(name = "nb_ls_ind_recent") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts collectifs parmi les LS récents xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime - 5) & TYPECONST == "C") %>%
    dplyr::group_by(DEPCOM) %>%
    dplyr::count(name = "nb_ls_coll_recent") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts étudiants parmi les LS récents xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime-5) & TYPECONST == "E") %>%
    dplyr::group_by(DEPCOM) %>%
    dplyr::count(name = "nb_ls_etu_recent") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts individuels parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & TYPECONST == "I" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_ind_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts collectifs parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & TYPECONST == "C" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_coll_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts étudiants parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & TYPECONST == "E" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_etu_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts individuels récents parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime - 5) & TYPECONST == "I" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_ind_recent_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts collectifs récents parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime - 5) & TYPECONST == "C" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_coll_recent_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logts étudiants récents parmi les LS en QPV xx
  table <- data %>%
    dplyr::filter(MODE != "9" & LOCAT >= (millesime - 5) & TYPECONST == "E" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_etu_recent_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # nb de mises en service en QPV
  table <- data %>%
    dplyr::filter(MODE != "9" & MES_SANSCUMUL == 1 & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_mes_qpv") %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # âge moyen du parc des LS
  table <- data %>%
    dplyr::filter(MODE != "9") %>%
    dplyr::group_by(DEPCOM) %>%
    summarise(age_somme = sum(age, na.rm = T),
              nb_ls_age_connu = sum(!is.na(age))) %>%
    # mutate_if(is.numeric, round, 0) %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # âge moyen du parc par DPE énergétique
  table <- data %>%
    dplyr::filter(MODE != "9" & !is.na(age) & DPEENERGIE %in% c("A", "B", "C", "D", "E", "F", "G")) %>%
    dplyr::select(DEPCOM, DPEENERGIE, age_somme_dpe_ener = age) %>%
    dplyr::mutate(nb_ls_age_connu_dpe_ener = 1) %>%
    tidyr::pivot_wider(names_from = DPEENERGIE, values_from = c(age_somme_dpe_ener, nb_ls_age_connu_dpe_ener), names_sort = TRUE, names_sep = "_",
                       values_fn = sum) %>%
    dplyr::mutate_if(is.numeric, round, 0) %>%
    dplyr::left_join(table, ., by = "DEPCOM")


  # âge moyen du parc des LS en QPV
  table <- data %>%
    dplyr::filter(MODE != "9" & QPV == "1") %>%
    dplyr::group_by(DEPCOM) %>%
    summarise(age_somme_qpv = sum(age, na.rm = T),
              nb_ls_qpv_age_connu = sum(!is.na(age))) %>%
    mutate_if(is.numeric, round, 0) %>%
    dplyr::left_join(table, ., by = "DEPCOM")

  # part des logements sociaux où un DPE est réalisé
  table <- data %>%
    dplyr::filter(MODE != "9" & DPEDATE %not_in% c("", "01/1900", NA)) %>%
    dplyr::group_by(DEPCOM) %>%
    count(name = "nb_ls_dpe_realise") %>%
    left_join(table, ., by = "DEPCOM")


  # taux de vacance structurelle par ancienneté des logements (mêmes tranches qu'au chap 4)

  # Ajoute la colonne manquante si elle n'est pas là pour le rename
  cols <- c(`1` = NA_real_, `2` = NA_real_, `3` = NA_real_, `4` = NA_real_, `5` = NA_real_, `6` = NA_real_)

    table <- data %>%
    dplyr::filter(duree_vacance > 3 & MODE == "2" & !is.na(duree_vacance)) %>%
    group_by(DEPCOM) %>%
    count(age2) %>% spread(age2, n) %>%
    add_column(!!!cols[!names(cols) %in% names(.)]) %>%
    rename(
      num_vac_struct_age_0_5 = `1`,
      num_vac_struct_age_5_10 = `2`,
      num_vac_struct_age_10_20  = `3`,
      num_vac_struct_age_20_40 = `4`,
      num_vac_struct_age_40_60 = `5`,
      num_vac_struct_age_60_plus = `6`
    ) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE %in% c("1", "2")) %>%
    group_by(DEPCOM) %>%
    count(age2) %>% spread(age2, n) %>%
    add_column(!!!cols[!names(cols) %in% names(.)]) %>%
    rename(
      denom_vac_struct_age_0_5 = `1`,
      denom_vac_struct_age_5_10 = `2`,
      denom_vac_struct_age_10_20  = `3`,
      denom_vac_struct_age_20_40 = `4`,
      denom_vac_struct_age_40_60 = `5`,
      denom_vac_struct_age_60_plus = `6`
    ) %>%
    left_join(table, ., by = "DEPCOM")


  # taux de vacance structurelle en QPV /hors QPV
    cols_qpv <- c(`1` = NA_real_, `2` = NA_real_)

    if (millesime <= 2015){
    table <- table %>%
      add_column(num_vac_struct_qpv = NA_real_,
                 num_vac_struct_horsqpv = NA_real_,
                 denom_vac_struct_qpv = NA_real_,
                 denom_vac_struct_horsqpv = NA_real_)
  } else {
    table <- data %>%
      dplyr::filter(duree_vacance > 3 & MODE == "2" & !is.na(duree_vacance)) %>%
      group_by(DEPCOM) %>%
      count(QPV) %>% spread(QPV, n) %>%
      add_column(!!!cols_qpv[!names(cols_qpv) %in% names(.)]) %>%
      rename(num_vac_struct_qpv = `1`,
             num_vac_struct_horsqpv = `2`) %>%
      dplyr::left_join(table, ., by = "DEPCOM")

    table <- data %>% dplyr::filter(MODE %in% c("1", "2")) %>%
      group_by(DEPCOM) %>%
      count(QPV) %>% spread(QPV, n) %>%
      add_column(!!!cols_qpv[!names(cols_qpv) %in% names(.)]) %>%
      rename(denom_vac_struct_qpv = `1`,
             denom_vac_struct_horsqpv = `2`) %>%
      dplyr::left_join(table, ., by = "DEPCOM")
  }


  # taux de mobilité selon le nb de pièces (regrouper 5 pièces et +)
  # Taux de mobilité= nb de logements ayant changé d'occupant/nombre de logements loués + vacants non mis en service
  table <- data %>%
    dplyr::mutate(annee_bail = lubridate::year(mdy(BAIL)),
                  NBPIECE = if_else(NBPIECE > 4, "num_mob_5_piece", paste0("num_mob_", NBPIECE, "_piece"))) %>%
    dplyr::filter(MODE == "1" & annee_bail == (millesime - 1) & LOCAT %not_in% c(millesime, millesime - 1)) %>%
    group_by(DEPCOM) %>%
    count(NBPIECE) %>%
    spread(NBPIECE, n) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::mutate(NBPIECE = if_else(NBPIECE > 4, "denom_mob_5_piece", paste0("denom_mob_", NBPIECE, "_piece"))) %>%
    dplyr::filter(MODE %in% c("1", "2") & LOCAT %not_in% c(millesime, millesime - 1)) %>%
    group_by(DEPCOM) %>%
    count(NBPIECE) %>%
    spread(NBPIECE, n) %>%
    left_join(table, ., by = "DEPCOM")


  # Ajoute la colonne manquante si elle n'est pas là pour le rename
  cols_finan <- c(nb_mes_plai_qpv = NA_real_, nb_mes_plus_qpv = NA_real_, nb_mes_pls_qpv = NA_real_, nb_mes_pli_qpv = NA_real_)

  if (millesime <= 2015) {
    table <- table %>%
      add_column(nb_mes_plai_qpv = NA_real_,
                 nb_mes_plus_qpv = NA_real_,
                 nb_mes_pls_qpv = NA_real_,
                 nb_mes_pli_qpv = NA_real_)
  } else {
    # répartition des mises en service selon le mode de financement dans les QPV
    table <- data %>%
      dplyr::filter(MES_SANSCUMUL == 1 & QPV == 1) %>%
      dplyr::mutate(value = 1, finan_cus = tolower(finan_cus)) %>%
      select(DEPCOM, finan_cus, value) %>%
      tidyr::pivot_wider(names_from = finan_cus, values_from = value, values_fn = sum, names_glue = "nb_mes_{finan_cus}_qpv") %>%
      add_column(!!!cols_finan[!names(cols_finan) %in% names(.)]) %>%
      left_join(table, ., by = "DEPCOM")
  }

  # part des PLAI selon l’origine du patrimoine pour les mises en service
  table <- data %>%
    dplyr::filter(MES_SANSCUMUL == 1 & finan_cus == "PLAI") %>%
    group_by(DEPCOM) %>%
    count(ORIGINE) %>% spread(ORIGINE, n) %>%
    rename(nb_plai_construit_org = `1`,
           nb_plai_acq_av_travaux = `2`,
           nb_plai_acq_ss_travaux = `3`,
           nb_plai_acq_vefa = `4`) %>%
    left_join(table, ., by = "DEPCOM")


  # prix moyen au m², Q1, Médiane, Q3 pour le parc total
  table <- data %>%
    dplyr::filter(MODE == "1" & !is.na(LOYERPRINC) & !is.na(SURFHAB)) %>%
    group_by(DEPCOM) %>%
    summarize(somme_loyer = sum(LOYERPRINC, na.rm = T),
              somme_surface = sum(SURFHAB, na.rm = T)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[2]) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q2") %>%
    dplyr::select(DEPCOM, nb_loymoy_q2) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[3]) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_mediane") %>%
    dplyr::select(DEPCOM, nb_loymoy_mediane) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy > quartiles[4]) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q4") %>%
    dplyr::select(DEPCOM, nb_loymoy_q4) %>%
    left_join(table, ., by = "DEPCOM")

  # prix moyen au m², Q1, Médiane, Q3 pour le parc récent
  table <- data %>%
    dplyr::filter(MODE == "1" & LOCAT >= (millesime - 5) & !is.na(SURFHAB)) %>%
    group_by(DEPCOM) %>%
    summarize(somme_loyer_recent = sum(LOYERPRINC, na.rm = T)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & LOCAT >= (millesime - 5) & !is.na(LOYERPRINC)) %>%
    group_by(DEPCOM) %>%
    summarize(somme_surface_recent = sum(SURFHAB, na.rm = T)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[2] & LOCAT >= (millesime - 5)) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q2_recent") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_q2_recent)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[3] & LOCAT >= (millesime - 5)) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_mediane_recent") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_mediane_recent)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy > quartiles[4] & LOCAT >= (millesime - 5)) %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q4_recent") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_q4_recent)) %>%
    left_join(table, ., by = "DEPCOM")

  # prix moyen au m², Q1, Médiane, Q3 pour le parc total en QPV
  table <- data %>%
    dplyr::filter(MODE == "1" & QPV == "1" & !is.na(LOYERPRINC)) %>%
    group_by(DEPCOM) %>%
    summarize(somme_surface_enqpv = sum(SURFHAB, na.rm = T)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & QPV == "1" & !is.na(SURFHAB)) %>%
    group_by(DEPCOM) %>%
    summarize(somme_loyer_enqpv = sum(LOYERPRINC, na.rm = T)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[2] & QPV == "1") %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q2_enqpv") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_q2_enqpv)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy < quartiles[3] & QPV == "1") %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_mediane_enqpv") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_mediane_enqpv)) %>%
    left_join(table, ., by = "DEPCOM")

  table <- data %>%
    dplyr::filter(MODE == "1" & loymoy > quartiles[4] & QPV == "1") %>%
    group_by(DEPCOM) %>%
    count(name = "nb_loymoy_q4_enqpv") %>%
    dplyr::select(c(DEPCOM, nb_loymoy_q4_enqpv)) %>%
    left_join(table, ., by = "DEPCOM")


  # prix moyen au m² selon l’ancienneté du logement
   table <- data %>%
     dplyr::filter(MODE == "1" & !is.na(SURFHAB) & !is.na(LOYERPRINC)) %>%
     dplyr::mutate(age = case_when(age < 5            ~ "age_inf_5",
                                   age %[)% c(5, 10)  ~ "age_5_10",
                                   age %[)% c(10, 20) ~ "age_10_20",
                                   age %[)% c(20, 40) ~ "age_20_40",
                                   age %[)% c(40, 60) ~ "age_40_60",
                                   age >= 60          ~ "age_60_plus")) %>%
     group_by(DEPCOM, age) %>%
     summarise(somme_loyer = sum(LOYERPRINC, na.rm = TRUE),
               somme_surface = sum(SURFHAB, na.rm = TRUE), .groups = "drop") %>%
     pivot_wider(names_from = age, values_from = c(somme_loyer, somme_surface), names_glue = "{.value}_{age}", names_sort = TRUE) %>%
     left_join(table, ., by = "DEPCOM")


  return(table)
}
