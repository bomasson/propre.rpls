# [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html)

Copyright (c) 2020 MTES-MCT

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.


# Graphic charter  
By exception, the graphic charter of the french State mark is for the exclusive use of the actors of the state sphere.    
In particular, Marianne typography is protected by copyright.  
