test_that("creer_graphe_2_1 fonctionne", {
  indicateurs_rpls_illustrations <- lire_rpls_exemple() %>%
    dplyr::filter(Zone_ref)

  graph <- creer_graphe_2_1(data = indicateurs_rpls_illustrations, annee = 2019)
  testthat::expect_equal(attr(graph, "class"), c("girafe", "htmlwidget", "ggiraph"))
})
