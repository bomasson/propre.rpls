test_that("creer_carte_6_1 fonctionne", {
  indicateurs_rpls <- lire_rpls_exemple()

  # Test que la carte est un objet html
  objet <- creer_carte_6_1(data = indicateurs_rpls, annee = 2019, carto = mapfactory::fond_carto("Corse"),
                           bornes = c(4, 5, 6))

  testthat::expect_is(objet, "htmlwidget")
})
