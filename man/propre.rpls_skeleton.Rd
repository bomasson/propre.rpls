% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/skeleton.R
\name{propre.rpls_skeleton}
\alias{propre.rpls_skeleton}
\title{Fonction de creation du repertoire du projet de l'utilisateur}
\usage{
propre.rpls_skeleton(path, ...)
}
\arguments{
\item{path}{Repertoire du projet de publication saisi par l'utilisateur.}

\item{...}{Autres parametres saisis par l'utilisateur (region, epci, millesime).}
}
\value{
TRUE
}
\description{
A partir de la saisie des parametres par l'utilisateur, cette fonction peuple le repertoire de travail de l'utilisateur avec
le bookdown et fixe les parametres yaml dans \code{index.Rmd}.
}
\examples{
\dontrun{
 propre.rpls_skeleton("rpls2019bretagne", annee="2019",
                       epci_ref="1- Tous les EPCI de la zone", nom_region="Bretagne")
}

}
