% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/creer_carte_6_1.R
\name{creer_carte_6_1}
\alias{creer_carte_6_1}
\title{Creation de la carte represantant les loyers moyens au m2 par EPCI (chap Loyers et financements).}
\usage{
creer_carte_6_1(
  data,
  annee,
  carto,
  bornes = NULL,
  palette = "pal_gouv_i",
  inverse = TRUE,
  maille = "EPCI",
  titre = "Loyer moyen par {maille} \\nau 1er janvier {annee}",
  note_de_lecture = "",
  na_label = "Valeurs manquantes",
  ...
)
}
\arguments{
\item{data}{La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.}

\item{annee}{Une annee, parmi les millesimes selectionnables par l'utilisateur, au format numérique.}

\item{carto}{La table des fonds de carte realisee avec \code{mapfactory::\link{fond_carto}}.}

\item{bornes}{Les bornes manuelles.}

\item{palette}{Choix de la palette de couleurs parmi celle de \code{gouvdown::\link{scale_color_gouv_discrete}}}

\item{inverse}{Choix du sens de la progression des couleurs : du plus sombre au plus clair (FALSE) ou du plus clair au plus sombre (TRUE)}

\item{maille}{Le maillage souhaite pour la carte, a choisir parmi "commune", "EPCI" ou "département". "EPCI" par defaut.}

\item{titre}{Une chaine de caractère si vous voulez ajouter un titre specifique. (par défaut: "Loyer moyen par {maille} au 1er janvier {annee}")}

\item{note_de_lecture}{Une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources}

\item{na_label}{L'etiquette à afficher dans la legende pour les valeurs manquantes ("Valeurs manquantes" par défaut).}

\item{...}{Autres paramètres de la fonction \href{https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/mapfactory/reference/creer_carte.html}{\code{mapfactory::creer_carte}}}
}
\value{
Une carte choroplethe mise en page au format html.
}
\description{
Creation de la carte des loyers moyens/m² par EPCI.
}
\examples{
indicateurs_rpls <- lire_rpls_exemple()

creer_carte_6_1(data = indicateurs_rpls,
                annee = 2019,
                carto = mapfactory::fond_carto("Corse"),
                bornes = NULL,
                note_de_lecture = "")
}
