# propre.rpls 0.6.2
* Correction des données 2022 au niveau des QPV.
* Amelioration verbatim_6 : ajout de la comparaison France pour le loyer moyen.
* Implémentation des noms des financements spécifiques des DROMs pour les fonctions du chapitre 6.
* Les fonctions de créations de cartes s'appuient désormais sur un package dédié [{mapfactory}](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/mapfactory/). 
* Amélioration des cartes : introduction d'un paramètre `na_label` pour contrôler l'affichage de la borne `NA` dans les légendes, le titre par défaut des carte à la commune a été corrigé. La fonction `get_fond_carto()` prend un nouveau paramètre `ombre` pour contrôler l'écartement de l'ombre régionale.
* La fonction `creer_pdf_book()` accepte désormais un facteur de mise à l'échelle `scale` pour contrôler la mise en page des tableaux dans le book pdf.

# propre.rpls 0.6.1.9999
* Suppression de la fonction `discretiser()` remplacée par celle de `{mapfactory}`.
* Correction de la dépendance orpheline à `santoku::lbl_format()`

# propre.rpls 0.6.0.9999

* Injections des résultats RPLS 2022.
* Mise à jour des zonages spécifiques.
* Mise à jour des données logements issues du RP INSEE.

# propre.rpls 0.5.1

* Correction données 2021 aux niveau des DPE réalisés (taux et nombre de dpe par étiquette)
* Mise à jour de la liste des mainteneuses du package.
* Amélioration de la présentation des fonctions `get_dataprep()`, `get_fond_carto()`.

# propre.rpls 0.5.0  

* Correction du bug de get_dataprep() et get_fond_carto() .
* Incrémentation du numéro de version (passage à 5.x) pour signifier le changement de millésime.

# propre.rpls 0.4.3

* Implémentation du paramétrage par l'utilisateur des fonctions de dataviz au niveau du titre et de l'éventuelle note de lecture.
* Suppression des mentions automatiques précédent les tableaux "Tableau X.X:"
* Implémentation du paramétrage par l'utilisateur de l'ajout d'une barre de défilement verticale sur chaque fonction `creer_tableau_X_X()`
* Réduction du temps de (re)compilation des books html grâce à un export des résultats de la datapréparation (nouvelles fonctions `get_dataprep()`, `get_fond_carto()`). 
* Implémentation de zonages spécifiques (EPT en IdF et Métropole de Lyon en AURA et révision de l'ordre d'affichage des EPCI (par type d'EPCI, puis par ordre alphabétique). 
* Ajout de la fonction `creer_pdf_book()` pour exporter la publication au format pdf. 
* Implémentation du paramétrage par l'utilisateur du popover des cartes (choix d'un indicateur complémentaire à l'indicateur cartographié possible)
* Paramétrage des fonctions de création de carte selon la maille d'analyse désirée (commune, EPCI ou département)
* Adaptation des fonctions de création de carte pour améliorer la lisibilité : la barre d'échelle et la fleche du nord ne masquent plus le territoire
* Implémentation des mentions légales et autres formulations vernaculaires régionales à partir de {propre.datareg}
* Ajout dans l'app shiny d'une note concernant le zonage "Communes"
* Modification de run_rpls_explorer() pour n'afficher que les communes ayant au moins 1 logement social
* Modification de la fonction `creer_verbatim_1()` pour ajouter la comparaison de l'évolution du parc de LS par rapport à celle du parc de rp.
* Modification de DESCRIPTION: ajout de `LazyDataCompression: bzip2` pour enlever le warning dans le check avec R >=4.1.1
* Modification de la fonction `creer_graph_1_1()` pour ajouter une année de recul dans la courbe d'évolution.
* Modification des catégories de financements pour coller à la circulaire [CUS du 12 avril 2010 (p.28)](http://www.financement-logement-social.logement.gouv.fr/IMG/pdf/cus_circulaire_12-avr-2010_cle152934-1.pdf)
* Ajout d'indicateurs : 'Nombre de résidences principales en location du parc privé' et sa ventilation par nombre de pièces, nombre de résidences principales 5 ans auparavant 
* Implémentation des nouveaux millésimes : recensement INSEE 2018, RPLS 2021  
* Ajout du nombre de logement total au 1er janvier dans la boite de dialogue de la carte du chapitre 1 au survol d'un EPCI

# propre.rpls 0.4.2

* correction du titre du graphique 3.1, des tableaux 1.1 et 3.1 pour la Réunion et Mayotte
* correction du calcul de l'âge moyen des logements d'étiquette énergie C dans les indicateurs complémentaires
* ajout d'une fonction arrange_zonage pour trier les territoires sur les tableaux. Cette fonction est appelée dans index.rmd  
* modification de 10-methodo.Rmd aux niveau des liens : ajout du lien vers l'article L.441-2 CCH et modification du lien pour l'article L.481-1 CCH (qui pointait vers celui du L.441-2 CCH).  

# propre.rpls 0.4.1

* Ajustement des titres d'illustration pour la Réunion et Mayotte
* Uniformisation du **formatage des chiffres** (suppression du . anglosaxon pour les décimales, maintien d'un zéro après la virgule si la précision est d'un chiffre apès la virgule)
* Amélioration du **graphique 4.1 sur les DPE** : précision dans le champ couvert (DPE renseigné au lieu de DPE réalisé) et ajout d'un fond bland derrière les étiquettes
* Correction **calcul de la densité** de logements sociaux et du nombre de résidences principales
* Correction ligne avec DEPCOM à vide sur tab_result en 2016
* Modification de la numérotation des tableaux (Tableau au lieu de table)
* Correction du calcul du **nb de logements du parc récent**

# propre.rpls 0.4.0

* Amélioration du graphique 4.1 sur la performance énergétique du parc : les couleurs correspondents aux couleurs usuelles pour les DPE
* Correction verbatim 4 : simplification de l'intertitre et modification du dénominateur dans le calcul des pourcentages  
* Correction des indicateurs liés aux nombre de résidence principales (densités, nombre de pièces des RP)  
* Correction et précision du verbatim 6 sur le calcul des rangs : le rang est bien calculé et donne le rang en fonction des régions les plus chers  
* Correction du calcul de l'évolution du nombre de logements sociaux N/N-1 pour le tableau du chap 1  
* Correction du calcul des sorties du parc sur le tableau 3.1  
* Correction du calcul des âges moyens, du nombre de logements couverts par un DPE
* Intégration de COGiter 0.0.5 pour rpls_exemple
* suppression des lignes des arrondissements de Paris, Lyon et Marseille qui donnait le double des données pour ces villes
* Création d'une fonction `creer_carte()` qui permet de générer une carte sur tout indicateur de RPLS
* Création d'une fonction `fond_carto()` qui permet de générer une liste des fonds de carte nécessaires pour creer_carte()
* Suppression de fond_carto_dep() et remplacement de `fond_carto_epci()` par un alias sur `fond_carto()`
* Amélioration visuelle des cartes produites
* Modification du template avec ces nouvelles cartes. Cela produit 4 changements sur les pages Index.Rmd pour la création du fond de carte, et sur 01-evol_parc.Rmd, 05-tensions.Rmd et 06-loyers_financmts.Rmd pour l'appel des cartes.
* Modification du template au niveau du verbatim du chap 6 : modification des arguments data (`indicateurs_rpls` au lieu de `indicateurs_rpls_ref` et annee (`annee` au lieu de `2019`)
* Ajout de filtres de NA pour LOYERPRINC ET SURFHAB dans le calcul des loyers moyens (calculs des sommes des loyers et surfaces)
* Correction du calcul des nombres de pièces et et de la part de DPE du parc récent
* Correction du calcul nombre de sorties pour autre motif 
* Correction de la part de DPE réalisé


# propre.rpls 0.3.0  

* Ajout des données 2020
* Ajout des indicateurs complémentaires
* Structuration de la [page reference](https://rdes_dreal.gitlab.io/propre.rpls/reference/index.html) du package
* Mise à jour du theme du site du package
* Assemblage des vignettes par chapitre
* Reprise de la numérotation de la publication
* Ajout de fonctions de création des verbatims :  
  - `creer_verbatim_3()`
  - `creer_verbatim_4()`
  - `creer_verbatim_6()`
  - `creer_verbatim_mentions_legales()`
* Ajout du paramètre `epci` aux fonctions de création des tableaux pour afficher ou non des lignes EPCI
* Mise à jour du Guide de prise en main
* Passage au template bookdown de gouvdown
* Ajout d'un module shiny de consultation et d'export des données complémentaires, accessible par la fonction `run_rpls_explorer()`  
* Ajout du corpus de mentions légales
* Amélioration du verbatim du chap 1 pour mieux gérer les cas où le parc social recule

# propre.rpls 0.2.0

* Rédaction de la vignette Guide de prise en main.
* Peuplement du squelette bookdown avec les fonctions de création des illustrations.
* Ajout de la charte graphique Etat via {gouvdown}.
* Ajout de fonctions de création des illustrations :  

  - `creer_graphe_1_1()`  
  - `creer_tableau_1_1()`  
  - `creer_carte_1_1()`  
  - `creer_graphe_2_1()`  
  - `creer_graphe_3_1()`  
  - `creer_graphe_3_2()`  
  - `creer_tableau_3_1()`  
  - `creer_tableau_4_1()`  
  - `creer_graphe_4_1()`  
  - `creer_tableau_5_1()`  
  - `creer_carte_5_1()`  
  - `creer_carte_5_2()`  
  - `creer_graphe_6_1()`  
  - `creer_graphe_6_2()`  
  - `creer_carte_6_1()`  

* Ajout de la fonction `discretiser()` pour discrétiser les variables numériques pour les cartes.
* Ajout des fonctions `format_fr_nb` et `format_fr_pct` pour mettre en forme les chiffres et les pourcentages dans la publication.
* Ajout de la fonction `lire_rpls_exemple()` pour récupérer un jeu d'exemple.
* Ajout de la fonction `caption()` pour uniformiser la mention des sources dans les illustrations.
* Mise à jour des données du RP (passage à 2017).
* Ajout de fonctions de création des verbatims :  

  - `creer_verbatim_1()`
  - `creer_verbatim_2()`
  - `creer_verbatim_5()`


# propre.rpls 0.1.1

* Ajout du site web pkgdown pour les branches master et de dév du projet.  
* Ajout de l'indication code coverage pour le projet.  
* Correction d'un bug sur la doc.   

# propre.rpls 0.1.0

* Ajout des vignettes d'explication de la data-préparation et de mode d'emploi du modèle de publication.  
* Ajout des fonctions de data-préparation `dataprep()`, `fond_carto_epci()`, `fond_carto_dep()`.  
* Ajout des jeux de données `tab_result` and `lgt_rp`.   
* Ajout du squelette de la publication bookdown.  
* Ajout du fichier `NEWS.md` pour suivre les évolutions du package.  
